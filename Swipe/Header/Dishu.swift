//
//  Dishu.swift
//  ComplyBag
//
//  Created by Apple on 09/01/21.
//

import UIKit

// MARK:- NAVIGATION TITLEs -
let LOGIN_PAGE_NAVIGATION_TITLE = "LOGIN"
let REGISTER_PAGE_NAVIGATION_TITLE = "REGISTER"
let UPLOAD_IMAGE_PAGE_NAVIGATION_TITLE = "UPLOAD PROFILE PICTURE"
let DASHBAORD_PAGE_NAVIGATION_TITLE = "DASHBOARD"
let NEW_BAG_PAGE_NAVIGATION_TITLE = "NEW BAG"
let SCAN_HISTORY_PAGE_NAVIGATION_TITLE = "SCAN HISTORY"
let CHANGE_PASSWORD_PAGE_NAVIGATION_TITLE = "CHANGE PASSWORD"

// MARK:- ALL TEXT -
// continue as a
let continueAsa_TEXT_SignIn = "Sign In"
let continueAsa_TEXT_CreateAnAccount = "Create an Account"

// login page
let loginPage_TEXT_SignIn = "Sign In"
let loginPage_TEXT_ForgotPassword = "Forgot Password"
let loginPage_TEXT_DontHaveAnAccount = "Don't have an account ? - Sign Up"

// register
let registrationPage_TEXT_DontHaveAnAccount = "Continue"

//upload profile
let uploadprofilePage_TEXT_Camera = "Camera"
let uploadprofilePage_TEXT_Gallery = "Gallery"

// dahbaord
let dashboard_TEXT_NewBag = "New Bag"
let dashboard_TEXT_ScanReport = "Scan Report"

// submit bag form
let submitBagPage_TEXT_Submit = "Submit"

class Dishu: NSObject {
    
     
    
    class func buttonStyle(button:UIButton,bCornerRadius:CGFloat,bBackgroundColor:UIColor,bTitle:String,bTitleColor:UIColor) {
        button.layer.cornerRadius = bCornerRadius
        button.clipsToBounds = true
        button.backgroundColor = bBackgroundColor
        button.setTitle(bTitle, for: .normal)
        button.setTitleColor(bTitleColor, for: .normal)
    }
    
}
