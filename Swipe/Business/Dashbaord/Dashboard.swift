//
//  Dashboard.swift
//  Swipe
//
//  Created by Apple on 13/10/20.
//  Copyright © 2020 Apple . All rights reserved.
//

import UIKit

class Dashboard: UIViewController {

    var dictOfNotificationPopup:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "DASHBOARD"
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton!
    @IBOutlet weak var btnEdit:UIButton!
    
    @IBOutlet weak var btnStopSchedule:UIButton! {
        didSet {
            btnStopSchedule.layer.cornerRadius = 4
            btnStopSchedule.clipsToBounds = true
            btnStopSchedule.backgroundColor = UIColor.init(red: 240.0/255.0, green: 210.0/255.0, blue: 70.0/255.0, alpha: 1) // 240, 210, 70
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
     @IBOutlet weak var btnGroup:UIButton!
    
    @IBOutlet weak var btnNewRequest:UIButton! {
        didSet {
            btnNewRequest.setTitle("Send Money", for: .normal)
            btnNewRequest.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            btnNewRequest.layer.cornerRadius = 6
            btnNewRequest.clipsToBounds = true
            btnNewRequest.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBOutlet weak var btnDeliveredHistory:UIButton! {
        didSet {
            btnDeliveredHistory.setTitle("Cards", for: .normal)
            btnDeliveredHistory.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            btnDeliveredHistory.layer.cornerRadius = 6
            btnDeliveredHistory.clipsToBounds = true
            btnDeliveredHistory.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBOutlet weak var imgVieww:UIImageView!
    
    @IBOutlet weak var switchh:UISwitch! {
        didSet {
            switchh.isHidden = true
        }
    }
    
    @IBOutlet weak var bottomVieww:UIView! {
        didSet {
            bottomVieww.backgroundColor = .clear
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // self.sideBarMenuClick()
        
        // self.btnGroup.addTarget(self, action: #selector(jobHistoryClickMethod), for: .touchUpInside)
        self.btnEdit.addTarget(self, action: #selector(editClickMethod), for: .touchUpInside)
        
        self.btnDeliveredHistory.addTarget(self, action: #selector(shopNowClickMethod), for: .touchUpInside)
        self.btnNewRequest.addTarget(self, action: #selector(orderClickMethod), for: .touchUpInside)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
          
            /*
             ["socialId": , "fullName": ios1, "image": , "userId": 44, "socialType": , "firebaseId": , "role": Member, "device": Android, "deviceToken": , "zipCode": , "address": , "lastName": , "OTP": , "email": ios@gmail.com, "contactNumber": 9856325698, "wallet": 0]
             (lldb)
             */
            
            self.lblTitle.text      = "Welcome, "+(person["fullName"] as! String)
            
            imgVieww.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "logo"))
            
        } else {
            // self.plusDriverLogin()
        }
        
        self.switchh.addTarget(self, action: #selector(switchhClickMethod), for: .valueChanged)
        // self.btnStopSchedule.addTarget(self, action: #selector(pushToDashboardPD), for: .touchUpInside)
       
        
        self.sideBarMenuClick()
    }
    
    @objc func orderClickMethod() {
        
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyRequestMoney")
        defaults.setValue(nil, forKey: "keyRequestMoney")
        
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SendMoneyId") as? SendMoney
         self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    // MARK:- JOB HISTORY -
    @objc func shopNowClickMethod() {

          let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageCardsId") as? ManageCards
          self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    // MARK:- EDIT -
    @objc func editClickMethod() {
        
          let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditProfileId") as? EditProfile
          self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    @objc func switchhClickMethod() {
        
        if switchh.isOn {
            print("on")
            // self.availaibilityOnOrOff(strOnOff: "1")
        } else {
            print("off")
            // self.availaibilityOnOrOff(strOnOff: "0")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    // MARK:- PUSH TO SPOT SCHEDULING -
    @objc func pushToDashboardPD() {
         // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDSpotSchedulingId") as? PDSpotScheduling
         // self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    
}
