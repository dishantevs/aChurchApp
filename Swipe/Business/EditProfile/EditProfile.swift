//
//  EditProfile.swift
//  Swipe
//
//  Created by evs_SSD on 1/10/20.
//  Copyright © 2020 Apple . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import CRNotifications
import SDWebImage

// MARK:- LOCATION -
import CoreLocation

class EditProfile: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {

    var imgUploadYesOrNo:String!
    
    var str_check_upload_Section_select:String! = "0"
    
    var str_type_one : String!
    
    // image
    var imageStr:String!
    var imgData:Data!
    
    var imgUploadYesOrNo2:String! = "0"
    var imageStr2:String!
    var imgData2:Data!
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    // address line is
    var strAddressLineIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
           didSet {
            let defaults = UserDefaults.standard
            let userName = defaults.string(forKey: "KeyLoginPersonal")
            if userName == "loginViaPersonal" {
                // personal user
                navigationBar.backgroundColor = NAVIGATION_PERSONAL_BACKGROUND_COLOR
            }
            else {
               navigationBar.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            }
            
           }
       }
       
    @IBOutlet weak var lblDocumentType:UILabel! {
        didSet {
            lblDocumentType.textColor = .black
        }
    }
    
       @IBOutlet weak var lblNavigationTitle:UILabel! {
           didSet {
               lblNavigationTitle.text = "Edit Profile"
               lblNavigationTitle.textColor = .white
           }
       }
       
    @IBOutlet weak var btnBack:UIButton!
    
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            txtEmail.layer.cornerRadius = 4
            txtEmail.clipsToBounds = true
            txtEmail.isUserInteractionEnabled = false
            txtEmail.keyboardAppearance = .dark
        }
    }
    @IBOutlet weak var txtName:UITextField! {
        didSet {
            txtName.layer.cornerRadius = 4
            txtName.clipsToBounds = true
            txtName.keyboardAppearance = .dark
        }
    }
    @IBOutlet weak var txtPhoneNumber:UITextField! {
        didSet {
            txtPhoneNumber.layer.cornerRadius = 4
            txtPhoneNumber.clipsToBounds = true
            txtPhoneNumber.keyboardAppearance = .dark
            txtPhoneNumber.keyboardType = .numberPad
        }
    }
    
    @IBOutlet weak var txtAddress:UITextField! {
        didSet {
            txtAddress.layer.cornerRadius = 4
            txtAddress.clipsToBounds = true
            txtAddress.keyboardAppearance = .dark
            txtAddress.keyboardType = .default
        }
    }
    
    @IBOutlet weak var txtssn:UITextField! {
        didSet {
            txtssn.layer.cornerRadius = 4
            txtssn.clipsToBounds = true
            txtssn.keyboardAppearance = .dark
            txtssn.keyboardType = .default
        }
    }
    
    @IBOutlet weak var btnDOB:UIButton!
    @IBOutlet weak var txtdob:UITextField! {
        didSet {
            txtdob.layer.cornerRadius = 4
            txtdob.clipsToBounds = true
            txtdob.keyboardAppearance = .dark
            txtdob.keyboardType = .default
        }
    }
    
    @IBOutlet weak var btnSubmit:UIButton! {
        didSet {
            btnSubmit.setTitleColor(.white, for: .normal)
            btnSubmit.layer.cornerRadius = 4
            btnSubmit.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnEditBusinessDetails:UIButton! {
        didSet {
            btnEditBusinessDetails.setTitleColor(.white, for: .normal)
            btnEditBusinessDetails.layer.cornerRadius = 4
            btnEditBusinessDetails.clipsToBounds = true
            btnEditBusinessDetails.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            btnEditBusinessDetails.addTarget(self, action: #selector(editBusinessDetailsClickMethod), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 60
            imgProfile.clipsToBounds = true
            imgProfile.layer.borderColor = UIColor.lightGray.cgColor
            imgProfile.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var imgDocument:UIImageView! {
        didSet {
            imgDocument.layer.cornerRadius = 60
            imgDocument.clipsToBounds = true
            imgDocument.layer.borderColor = UIColor.lightGray.cgColor
            imgDocument.layer.borderWidth = 0.8
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.imgUploadYesOrNo = "0"
        
        self.strSaveLatitude = "0"
        self.strSaveLongitude = "0"
        
        btnBack.setImage(UIImage(named: "menuWhite"), for: .normal)
        sideBarMenu()
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EditProfile.uploadImageOpenActionSheet))

        imgProfile.isUserInteractionEnabled = true
        imgProfile.addGestureRecognizer(tapGestureRecognizer)
        
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(EditProfile.uploadImageOpenActionSheet2))

        imgDocument.isUserInteractionEnabled = true
        imgDocument.addGestureRecognizer(tapGestureRecognizer2)
        
        
        
        
        
        self.btnDOB.addTarget(self, action: #selector(dobClickMethod), for: .touchUpInside)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             print(person as Any)
            
            txtEmail.text = (person["email"] as! String) // email
            txtName.text = (person["fullName"] as! String) // name
            txtPhoneNumber.text = (person["contactNumber"] as! String) // phone
            
            txtssn.text = (person["ssn"] as! String)
            txtdob.text = (person["dob"] as! String)
            
            self.lblDocumentType.text = (person["document_type"] as! String)
            
            if (person["address"] as! String) == "" {
                
                // self.iAmHereForLocationPermission()
                
                txtAddress.text = (person["address"] as! String)
                
            } else {
                
                txtAddress.text = (person["address"] as! String) // address
            }
            
            self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            self.imgDocument.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            
            imgProfile.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "logo"))
            imgDocument.sd_setImage(with: URL(string: (person["documentUpload"] as! String)), placeholderImage: UIImage(named: "logo"))
            
        }
        else {
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:"Your Session has been Expired.", dismissDelay: 1.5, completion:{})
        }
        
        let userIcon = UIImage(named: "email")
        setPaddingWithImage(image: userIcon!, textField: txtEmail)
            
        let userIcon2 = UIImage(named: "user")
        setPaddingWithImage(image: userIcon2!, textField: txtName)
            
        let userIcon3 = UIImage(named: "phone")
        setPaddingWithImage(image: userIcon3!, textField: txtPhoneNumber)
            
        let userIcon4 = UIImage(named: "location")
        setPaddingWithImage(image: userIcon4!, textField: txtAddress)
        
        let userIcon5 = UIImage(systemName: "lock")
        setPaddingWithImage(image: userIcon5!, textField: txtssn)
            
        let userIcon6 = UIImage(systemName: "lock")
        setPaddingWithImage(image: userIcon6!, textField: txtdob)
        
        btnSubmit.addTarget(self, action: #selector(editProfileTextScreenValidation), for: .touchUpInside)
        btnSubmit.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        btnEditBusinessDetails.isHidden = true
        
    }
    
    @objc func dobClickMethod() {
        
        RPicker.selectDate(title: "Select Date", didSelectDate: {[] (selectedDate) in
            // TODO: Your implementation for date
            self.txtdob.text = selectedDate.dateString("dd-MM-yyyy")
        })
        
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            // print("local address ==> "+localAddress as Any) // south west delhi
            // print("local address mini ==> "+localAddressMini as Any) // new delhi
            // print("locality ==> "+locality as Any) // sector 10 dwarka
            
            // print(self.strSaveCountryName as Any) // india
            // print(self.strSaveStateName as Any) // new delhi
            // print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            self.txtAddress.text = self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini
        }
    }
    
    @objc func editBusinessDetailsClickMethod() {
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditBusinessDetailsId") as? EditBusinessDetails
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func sideBarMenu() {
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func setPaddingWithImage(image: UIImage, textField: UITextField){
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 50))
        imageView.frame = CGRect(x: 13.0, y: 13.0, width: 24.0, height: 24.0)
        //For Setting extra padding other than Icon.
        let seperatorView = UIView(frame: CGRect(x: 50, y: 0, width: 2, height: 50))
        seperatorView.backgroundColor = UIColor(red: 80/255, green: 89/255, blue: 94/255, alpha: 1)
        view.addSubview(seperatorView)
        textField.leftViewMode = .always
        view.addSubview(imageView)
        view.backgroundColor = .clear
        textField.leftViewMode = UITextField.ViewMode.always
        textField.leftView = view
    }
    
    @objc func uploadImageOpenActionSheet() {
        let alert = UIAlertController(title: "Upload image", message: "Camera or Gallery", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
             self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
             self.openGallery()
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler:{ (UIAlertAction)in
            //print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            //print("completion block")
        })
    }
    
    @objc func openCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func openGallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if self.imgUploadYesOrNo == "3" {
            
            
            let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                imgDocument.image = image_data // show image on image view
            let imageData:Data = image_data!.pngData()!
            imageStr2 = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
                
            imgData2 = image_data!.jpegData(compressionQuality: 0.2)!
            
            self.update_document_type_wb()
        } else {
            
            self.imgUploadYesOrNo = "1"
                
                // let indexPath = IndexPath.init(row: 0, section: 0)
                // let cell = tbleView.cellForRow(at: indexPath) as! EditProfileTableCell
                
            let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                imgProfile.image = image_data // show image on image view
            let imageData:Data = image_data!.pngData()!
            imageStr = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
                
            imgData = image_data!.jpegData(compressionQuality: 0.2)!
            
        }
        

    }
    
    // MARK:- EDIT PROFILE WEBSERVICE -
    @objc func editProfileTextScreenValidation() {

        if txtName.text == "" {
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strNameValidation, dismissDelay: 1.5, completion:{})
        }
        else
        if txtPhoneNumber.text == "" {
            CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strPhoneValidation, dismissDelay: 1.5, completion:{})
        }
        else {
            if self.imgUploadYesOrNo == "1" {
                 
                self.editWithImage()
            } else {
            
                self.editProfileWB()
            }
            
        }
        
    }
    
    @objc func editProfileWB() {
        
        self.view.endEditing(true)
        
        Utils.RiteVetIndicatorShow()
        
        let urlString = BASE_URL_aChurchapp
               
        var parameters:Dictionary<AnyHashable, Any>!
       
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            let x : Int = (person["userId"] as! Int)
            let myString = String(x)
            
            parameters = [
                "action"        : "editprofile",
                "userId"        : String(myString),
                "fullName"       : String(txtName.text!),
                "contactNumber"   : String(txtPhoneNumber.text!),
                "address"   : String(txtAddress.text!),
                
                "ssn"   : String(txtssn.text!),
                "dob"   : String(txtdob.text!)
                
                    
            ]
        }
                
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
               
            switch(response.result) {
                case .success(_):
                    if let data = response.result.value {

                               
                        let JSON = data as! NSDictionary
                                 print(JSON)
                               
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                               
                        var strSuccessAlert : String!
                        strSuccessAlert = JSON["msg"]as Any as? String
                               
                        if strSuccess == "success" {
                                /*
                                 data =     {
                                     
                                 };
                                 msg = "Data save successfully.";
                                 status = success;
                                 */
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                            let defaults = UserDefaults.standard
                            defaults.setValue(dict, forKey: "keyLoginFullData")
                                   
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Message!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                                
                                    // self.loginWBClickMethod()
                                
                            
                            
                            
                            self.view.endEditing(true)
                                
                            Utils.RiteVetIndicatorHide()
                        }
                        else {
                                   // self.indicator.stopAnimating()
                                   // self.enableService()
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                            Utils.RiteVetIndicatorHide()
                        }
                               
                    }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                               // self.indicator.stopAnimating()
                               // self.enableService()
                Utils.RiteVetIndicatorHide()
                               
                let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                               
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                    NSLog("OK Pressed")
                }
                               
                alertController.addAction(okAction)
                               
                self.present(alertController, animated: true, completion: nil)
                               
                break
            }
        }
    
    }
    
    // MARK:- EDIT BUSINESS DETAILS WITH IMAGE -
    @objc func editWithImage() {
        self.view.endEditing(true)
        
        Utils.RiteVetIndicatorShow()
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
          
            // var parameters:Dictionary<AnyHashable, Any>!
                 let parameters = [
                     "action"           : "editprofile",
                     "userId"           : String(myString),
                     "fullName"         : String(txtName.text!),
                     "contactNumber"    : String(txtPhoneNumber.text!),
                     "address"          : String(txtAddress.text!),
                     
                     "ssn"   : String(txtssn.text!),
                     "dob"   : String(txtdob.text!)
                 ]
                
                    print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.imgData, withName: "image",fileName: "aChurchApp.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_aChurchapp)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            //print("Upload Progress: \(progress.fractionCompleted)")
                            
//                            let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)
//
//                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)
//
//                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
//                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
//
//                            alertController.view.addSubview(progressDownload)
//                            self.present(alertController, animated: true, completion: nil)
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                 let JSON = data as! NSDictionary
                                print(JSON)

                                var dict: Dictionary<AnyHashable, Any>
                                dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(dict, forKey: "keyLoginFullData")
                                
                                self.imgUploadYesOrNo = "0"
                                
                                 
                                
                                Utils.RiteVetIndicatorHide()
                                self.dismiss(animated: true, completion: nil)
                            }
                            else
                            {
                                Utils.RiteVetIndicatorHide()
                                CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:"Server Not Responding. Please try again Later.", dismissDelay: 1.5, completion:{})

                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        Utils.RiteVetIndicatorHide()
                        self.dismiss(animated: true, completion: nil)
                    }}}
        
    }
    
    
    
    @objc func uploadImageOpenActionSheet2() {
        
        let alert = UIAlertController(title: "Upload Documents", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Social Security Card", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.str_check_upload_Section_select = "1"
             
            self.imgUploadYesOrNo = "3"
            self.open_camera_gallery(str_title: "Social Security Card")
        }))
        
        alert.addAction(UIAlertAction(title: "Driving Licence", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.str_check_upload_Section_select = "2"
             
            self.imgUploadYesOrNo = "3"
            
            self.open_camera_gallery(str_title: "Driving Licence")
        }))
        
        alert.addAction(UIAlertAction(title: "Passport", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.str_check_upload_Section_select = "3"
             
            self.imgUploadYesOrNo = "3"
            
            self.open_camera_gallery(str_title: "Passport")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        
        
    }
    
    
    
    @objc func open_camera_gallery(str_title:String) {
        
        let alert = UIAlertController(title: "Upload "+String(str_title), message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            
            self.imgUploadYesOrNo = "3"
            self.openCamera2()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            
            self.imgUploadYesOrNo = "3"
            self.openGallery2()
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        
        
    }
    
    
    @objc func openCamera2() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func openGallery2() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
   
    
    
    @objc func update_document_type_wb() {
        self.view.endEditing(true)
        
        Utils.RiteVetIndicatorShow()
        
        if self.str_check_upload_Section_select == "0" {
            self.str_type_one = "none"
        } else if self.str_check_upload_Section_select == "1" {
            self.str_type_one = "Social Security Card"
        } else if self.str_check_upload_Section_select == "2" {
            self.str_type_one = "Driving Licence"
        } else if self.str_check_upload_Section_select == "3" {
            self.str_type_one = "Passport"
        }
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
          
            // var parameters:Dictionary<AnyHashable, Any>!
                 let parameters = [
                     "action"           : "editprofile",
                     "userId"           : String(myString),
                     "fullName"         : String(txtName.text!),
                     "contactNumber"    : String(txtPhoneNumber.text!),
                     "address"          : String(txtAddress.text!),
                     
                     "document_type" : (str_type_one!),
                     
                     "ssn"   : String(txtssn.text!),
                     "dob"   : String(txtdob.text!)
                 ]
                
                    print(parameters as Any)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.imgData2, withName: "documentUpload",fileName: "UpdateRegistrationDocument.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        
                        // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                        
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                to:BASE_URL_aChurchapp)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):

                        upload.uploadProgress(closure: { (progress) in
                            print("Upload Progress: \(progress.fractionCompleted)")
                            
                            /*let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)

                            let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

                            progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

                            alertController.view.addSubview(progressDownload)
                            self.present(alertController, animated: true, completion: nil)*/
                        })

                        upload.responseJSON { response in
                            //print(response.result.value as Any)
                            if let data = response.result.value {
                                 let JSON = data as! NSDictionary
                                print(JSON)

                                var dict: Dictionary<AnyHashable, Any>
                                dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(dict, forKey: "keyLoginFullData")
                                
                                 self.imgUploadYesOrNo2 = "0"
                                
                                if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                                    print(person as Any)
                                    
                                    self.lblDocumentType.text = (person["document_type"] as! String)
                                    
                                    self.imgDocument.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                                    self.imgDocument.sd_setImage(with: URL(string: (person["documentUpload"] as! String)), placeholderImage: UIImage(named: "logo"))
                                }
                                
                                Utils.RiteVetIndicatorHide()
                                self.dismiss(animated: true, completion: nil)
                            }
                            else {
                                Utils.RiteVetIndicatorHide()
                                CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:"Server Not Responding. Please try again Later.", dismissDelay: 1.5, completion:{})

                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        Utils.RiteVetIndicatorHide()
                        self.dismiss(animated: true, completion: nil)
                    }}}
        
    }
}
