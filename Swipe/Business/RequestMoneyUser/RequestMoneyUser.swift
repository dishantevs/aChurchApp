//
//  RequestMoneyUser.swift
//  Swipe
//
//  Created by Apple on 17/03/20.
//  Copyright © 2020 Apple . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import CRNotifications

class RequestMoneyUser: UIViewController {

    let cellReuseIdentifier = "requestMoneyUserTableCell"
    
    var arrListOfAllTransaction:Array<Any>!
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    var noDataLbl : UILabel?
    
    var clickedItemDict:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "REQUEST MONEY USER"
            lblNavigationTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    @IBOutlet weak var lblCurrentWalletBalance:UILabel! {
        didSet {
            lblCurrentWalletBalance.text = "      Current Wallet Balance          "
            lblCurrentWalletBalance.textColor = .white
            lblCurrentWalletBalance.backgroundColor = UIColor.init(red: 100.0/255.0, green: 206.0/255.0, blue: 225.0/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var lblTotalAmountInWallet:UILabel! {
        didSet {
            lblTotalAmountInWallet.textColor = .white
            lblTotalAmountInWallet.backgroundColor = UIColor.init(red: 100.0/255.0, green: 206.0/255.0, blue: 225.0/255.0, alpha: 1)
        }
    }
    @IBOutlet weak var tbleView: UITableView! {
            didSet {
                // tbleView.delegate = self
                // tbleView.dataSource = self
                self.tbleView.backgroundColor = .white
                self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))

            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        noDataLbl?.isHidden = true
        
        // sidebarMenuClick()
        
        let defaults = UserDefaults.standard
        if let name = defaults.string(forKey: "keySideBarMenu") {
            // print(name)
            if name == "dSideBar" {
                btnBack.setImage(UIImage(named: "menuWhite"), for: .normal)
                sidebarMenuClick()
            }
            else {
                btnBack.setImage(UIImage(named: "backs"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
        }
        else
        {
            btnBack.setImage(UIImage(named: "backs"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
        
        // let defaults = UserDefaults.standard
        let userName = defaults.string(forKey: "KeyLoginPersonal")
        if userName == "loginViaPersonal" {
            // personal user
            navigationBar.backgroundColor = NAVIGATION_PERSONAL_BACKGROUND_COLOR
             // self.view.backgroundColor = NAVIGATION_PERSONAL_BACKGROUND_COLOR
        }
        else {
            navigationBar.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            // self.view.backgroundColor = BUTTON_BACKGROUND_COLOR_BLUE
        }
        
        // self.allTransactionWB()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        /*if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let x : NSNumber = person["wallet"] as! NSNumber
            // self.lblTotalAmountInWallet.text = "$ "+"\(x)"
            
            if person["wallet"] is String {
                print("Yes, it's a String")

                self.lblTotalAmountInWallet.text = "$ "+(person["wallet"] as! String)

            } else if person["wallet"] is Int {
                print("It is Integer")
                            
                let x2 : Int = (person["wallet"] as! Int)
                let myString2 = String(x2)
                self.lblTotalAmountInWallet.text = "$ "+myString2
                            
            } else {
                print("i am number")
                            
                let temp:NSNumber = person["wallet"] as! NSNumber
                let tempString = temp.stringValue
                self.lblTotalAmountInWallet.text = "$ "+tempString
            }
            
        }
        else {
            // session expired. Please logout.
        }*/
        
        
        self.profileWB1()
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
                
        if scrollView == self.tbleView {
            let isReachingEnd = scrollView.contentOffset.y >= 0
                && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
            if(isReachingEnd) {
                if(loadMore == 1) {
                    loadMore = 0
                    page += 1
                    print(page as Any)
                    
                    self.usersListWBForSendOrReceive(strSearchKeyWord: page)
                    
                }
            }
        }
    }
    
    @objc func profileWB1() {
         Utils.RiteVetIndicatorShow()
           
        let urlString = BASE_URL_aChurchapp
               
        var parameters:Dictionary<AnyHashable, Any>!
           
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             let x : Int = (person["userId"] as! Int)
             let myString = String(x)
            
                parameters = [
                    "action"         : "profile",
                    "userId"         : String(myString)
                ]
         }
                
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                               
                    let JSON = data as! NSDictionary
                                  // print(JSON)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                    var strSuccessAlert : String!
                    strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == "success" {
                        Utils.RiteVetIndicatorHide()
                                
                                // self.lblBigAmount.text = "$ 0"
                                
                        var dict: Dictionary<AnyHashable, Any>
                        dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                        let defaults = UserDefaults.standard
                        defaults.setValue(dict, forKey: "keyLoginFullData")
                                
                                
                        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            
            // let x : NSNumber = person["wallet"] as! NSNumber
            // self.lblTotalAmountInWallet.text = "$ "+"\(x)"
            // self.myCurrentBalance = "\(x)"
           
            
                            if person["wallet"] is String {
                                print("Yes, it's a String")

                                self.lblTotalAmountInWallet.text = "$ "+(person["wallet"] as! String)

                            } else if person["wallet"] is Int {
                                print("It is Integer")
                            
                                let x2 : Int = (person["wallet"] as! Int)
                                let myString2 = String(x2)
                                self.lblTotalAmountInWallet.text = "$ "+myString2
                            
                            } else {
                                print("i am number")
                            
                                let temp:NSNumber = person["wallet"] as! NSNumber
                                let tempString = temp.stringValue
                                self.lblTotalAmountInWallet.text = "$ "+tempString
                            }
            
                            Utils.RiteVetIndicatorHide()
            
                            self.usersListWBForSendOrReceive(strSearchKeyWord: 1)
            
                        }
                        else {
                            self.lblTotalAmountInWallet.text = "$0"
                        }
                                
                // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BusinessDashbaordId") as? BusinessDashbaord
                // self.navigationController?.pushViewController(push!, animated: true)
                                
                                
                    }
                    else {
                                   // self.indicator.stopAnimating()
                                   // self.enableService()
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                        Utils.RiteVetIndicatorHide()
                    }
                               
                }

            case .failure(_):
                print("Error message:\(String(describing: response.result.error))")
                               // self.indicator.stopAnimating()
                               // self.enableService()
                Utils.RiteVetIndicatorHide()
                               
                let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                               
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                    NSLog("OK Pressed")
                }
                               
                alertController.addAction(okAction)
                               
                self.present(alertController, animated: true, completion: nil)
                               
                break
            }
        }
    
    }
    
    
    @objc func sidebarMenuClick() {
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- USERS LIST FOR PERSONAL -
    @objc func usersListWBForSendOrReceive(strSearchKeyWord:Int) {
            // Utils.RiteVetIndicatorShow()
            
            let defaults = UserDefaults.standard
            let userName = defaults.string(forKey: "KeyLoginPersonal")
            if userName == "loginViaPersonal" {
                // personal user
                 Utils.RiteVetIndicatorPesonalShow()
                
            }
            else {
                 Utils.RiteVetIndicatorShow()
            }
               
            let urlString = BASE_URL_aChurchapp
                   
            var parameters:Dictionary<AnyHashable, Any>!
               
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            let x : Int = (person["userId"] as! Int)
            let myString = String(x)
                
                /*
                 [action] => userlist
                 [userId] => 74
                 [pageNo] => 0
                 */
                    parameters = [
                        "action"         : "moneyrequestlist",
                        "userId"         : String(myString),
                        // "requestType"    : String(strSendOrReceive),
                        "pageNo"         : strSearchKeyWord
                    ]
             }
                    
        print("parameters-------\(String(describing: parameters))")
                       
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
                   
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                                   
                    let JSON = data as! NSDictionary
                    print(JSON)
                                    
                                   
                                   var strSuccess : String!
                                   strSuccess = JSON["status"]as Any as? String
                                   
                                   var strSuccessAlert : String!
                                   strSuccessAlert = JSON["msg"]as Any as? String
                                   
                                   if strSuccess == "success" //true
                                   {
                                    Utils.RiteVetIndicatorHide()
                                    
                                    // let defaults = UserDefaults.standard
                                    
                                     var ar : NSArray!
                                     ar = (JSON["data"] as! Array<Any>) as NSArray
                                     self.arrListOfAllTransaction = (ar as! Array<Any>)
                                    
                                    // self.searchArrayStr = strForCell
                                    
                                    // print(self.arrListOfSendReceiveUsers as Any)
                                    
                                    if self.arrListOfAllTransaction.count > 0 {
                                        self.noDataLbl?.isHidden = true
                                        self.noDataLbl = UILabel(frame: CGRect(x: 0, y: self.view.center.y, width: 290, height: 70))
                                        self.noDataLbl!.removeFromSuperview()
                                        self.tbleView.isHidden = false
                                        self.tbleView.delegate = self
                                        self.tbleView.dataSource = self
                                        self.tbleView.reloadData()
                                    } else {
                                        self.noDataLbl?.isHidden = false
                                        self.noDataLbl = UILabel(frame: CGRect(x: 0, y: self.view.center.y, width: 290, height: 70))
                                        self.noDataLbl?.textAlignment = .center
                                        self.noDataLbl?.font = UIFont(name: "Halvetica", size: 18.0)
                                        self.noDataLbl?.numberOfLines = 0
                                        self.noDataLbl?.text = "No Data Found"
                                        self.noDataLbl?.textColor = .black
                                        self.noDataLbl?.lineBreakMode = .byTruncatingTail
                                        self.noDataLbl?.center = self.view.center
                                        self.view.addSubview(self.noDataLbl!)
                                        self.tbleView.isHidden = true
                                    }
                                    
                                   }
                                   else
                                   {
                                       // self.indicator.stopAnimating()
                                       // self.enableService()
                                    CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                                       Utils.RiteVetIndicatorHide()
                                   }
                                   
                               }

                               case .failure(_):
                                   print("Error message:\(String(describing: response.result.error))")
                                   // self.indicator.stopAnimating()
                                   // self.enableService()
                                   Utils.RiteVetIndicatorHide()
                                   
                                   let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                                   
                                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                           UIAlertAction in
                                           NSLog("OK Pressed")
                                       }
                                   
                                   alertController.addAction(okAction)
                                   
                                   self.present(alertController, animated: true, completion: nil)
                                   
                                   break
                                }
                           }
            
            
        
           }
    
}

extension RequestMoneyUser: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllTransaction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RequestMoneyUserTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! RequestMoneyUserTableCell
        
        cell.backgroundColor = .white
        
        /*
         amount = 10;
         created = "Oct 13th, 2020, 6:36 pm";
         receiverId = 35;
         receiverImage = "http://demo2.evirtualservices.com/achurchapp/site/img/uploads/users/16018747281600772051869.jpg";
         receiverName = satish;
         receiverNumber = 9696868686;
         requestId = 8;
         senderId = 44;
         senderImage = "http://demo2.evirtualservices.com/achurchapp/site/img/uploads/users/1602585690aChurchApp.jpg";
         senderName = ios1;
         senderNumber = 9856325698;
         type = Sender;
         */
        
        let item = arrListOfAllTransaction[indexPath.row] as? [String:Any]
        // print(item as Any)
       
        if (item!["type"] as! String) == "Receiver" {
            
            cell.PersonNameLabel.text       = (item!["senderName"] as! String)
            cell.PersonMobileNOLabel.text   = (item!["senderNumber"] as! String)
            cell.statusLabel.textColor      = .systemRed
            cell.PersonImage.sd_setImage(with: URL(string: (item!["senderImage"] as! String)), placeholderImage: UIImage(named: "avatar"))
            
        } else {
            
            let backgroundView = UIView()
            backgroundView.backgroundColor = .clear
            cell.selectedBackgroundView = backgroundView

            cell.PersonNameLabel.text       = (item!["receiverName"] as! String)
            cell.PersonMobileNOLabel.text   = (item!["receiverNumber"] as! String)
            cell.statusLabel.textColor      = .systemGreen
            cell.PersonImage.sd_setImage(with: URL(string: (item!["receiverImage"] as! String)), placeholderImage: UIImage(named: "avatar"))
            
        }
        
        
        
        // let x : Int = (item!["receiverNumber"] as! Int)
        // let amountIs = String(x)
        
        if item!["amount"] is String {
                          
            print("Yes, it's a String")
          
            cell.statusLabel.text = "$ "+(item!["amount"] as! String)

        } else if item!["amount"] is Int {
          
            print("It is Integer")
          
            let x2 : Int = (item!["amount"] as! Int)
            let myString2 = String(x2)
            cell.statusLabel.text = "$ "+myString2
            
        } else {
        //some other check
          print("i am ")
          
            let temp:NSNumber = item!["amount"] as! NSNumber
            let tempString = temp.stringValue
            cell.statusLabel.text = "$ "+tempString
            
        }
        
         // my profile image
        
        // cell.statusLabel.text = "$ "+String(amountIs)
        
        
        /*
            // receive
            let contact = self.arrListOfAllTransaction[indexPath.row]
            let fbemail = ((contact as AnyObject)["userName"]! as? String ?? "")+" - $ "+((contact as AnyObject)["amount"]! as? String ?? "")
            cell.PersonNameLabel.text = fbemail
            
            let imagee = ((contact as AnyObject)["userImage"]! as? String ?? "")
            
            cell.PersonImage.sd_setImage(with: URL(string: imagee), placeholderImage: UIImage(named: "avatar")) // my profile image
            
            let phoneNumberIs = ((contact as AnyObject)["contactNumber"]! as? String ?? "")
            cell.PersonMobileNOLabel.text = phoneNumberIs
            
            
            cell.imgs.image = UIImage(named:"rightArrow")
            cell.imgs.isHidden = true
            
            
            // 1 : pending
            // 2 : send
            // 3 : decline
            
            let x : Int = (contact as AnyObject)["status"]! as! Int
            let amountIs = String(x)
            if amountIs == "1" {
                cell.statusLabel.isHidden = false
                cell.statusLabel.text = "pending..."
                cell.statusLabel.textColor = .white
                cell.statusLabel.backgroundColor = .systemGray
                cell.statusLabel.layer.cornerRadius = 2
                cell.statusLabel.clipsToBounds = true
            } else if amountIs == "2" {
                cell.statusLabel.isHidden = false
                cell.statusLabel.text = "accepted"
                cell.statusLabel.textColor = .white
                cell.statusLabel.backgroundColor = .systemGreen
                cell.statusLabel.layer.cornerRadius = 2
                cell.statusLabel.clipsToBounds = true
            } else if amountIs == "3" {
                cell.statusLabel.isHidden = false
                cell.statusLabel.text = "decline"
                cell.statusLabel.textColor = .white
                cell.statusLabel.backgroundColor = .systemRed
                cell.statusLabel.layer.cornerRadius = 2
                cell.statusLabel.clipsToBounds = true
            } else {
                cell.statusLabel.isHidden = true
            }
            */
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        // clickedItemDict
        
        let item = arrListOfAllTransaction[indexPath.row] as? [String:Any]
        print(item as Any)
        
        let x23 : Int = (item!["senderId"] as! Int)
        let myString23 = String(x23 )
        
        if (item!["type"] as! String) == "Receiver" {
            // red = push
            
            if item!["amount"] is String {
                              
                print("Yes, it's a String")
              
                let messageText = "Are your sure you want to pay $ "+(item!["amount"] as! String)+" to "+(item!["senderName"] as! String)
                let alert = UIAlertController(title: String("Confirm"), message: String(messageText), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Yes, Pay Now", style: UIAlertAction.Style.default, handler: { action in
                     
                    self.sendRequestedMoney(strReceiverId: myString23, strAmount: (item!["amount"] as! String))
                }))
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                     
                }))
                
                self.present(alert, animated: true, completion: nil)

            } else if item!["amount"] is Int {
              
                print("It is Integer")
              
                let x2 : Int = (item!["amount"] as! Int)
                let myString2 = String(x2)
                
                let messageText = "Are your sure you want to pay $ "+myString2+" to "+(item!["senderName"] as! String)
                let alert = UIAlertController(title: String("Confirm"), message: String(messageText), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Yes, Pay Now", style: UIAlertAction.Style.default, handler: { action in
                     
                    self.sendRequestedMoney(strReceiverId: myString23, strAmount: myString2)
                }))
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                     
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
            //some other check
              print("i am ")
              
                let temp:NSNumber = item!["amount"] as! NSNumber
                let tempString = temp.stringValue
                
                let messageText = "Are your sure you want to pay $ "+tempString+" to "+(item!["senderName"] as! String)
                let alert = UIAlertController(title: String("Confirm"), message: String(messageText), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Yes, Pay Now", style: UIAlertAction.Style.default, handler: { action in
                     
                    self.sendRequestedMoney(strReceiverId: myString23, strAmount: tempString)
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                     
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
            
            
        } else {
            // green = no push
        }
        
    }
    
    @objc func sendRequestedMoney(strReceiverId:String,strAmount:String) {
        //Utils.RiteVetIndicatorShow()
          
        let defaults = UserDefaults.standard
        let userName = defaults.string(forKey: "KeyLoginPersonal")
        if userName == "loginViaPersonal" {
            // personal user
             Utils.RiteVetIndicatorPesonalShow()
            
        }
        else {
             Utils.RiteVetIndicatorShow()
        }
        
        let urlString = BASE_URL_aChurchapp
               
        var parameters:Dictionary<AnyHashable, Any>!
           
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
             let x : Int = (person["userId"] as! Int)
             let myString = String(x)
            
                parameters = [
                    "action"         : "sendmoney",
                    "userId"         : String(myString),
                    "receiverId"     : String(strReceiverId),
                    "amount"         : String(strAmount)
                ]
                
            
             
         }
        
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
            response in
               
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {

                    let JSON = data as! NSDictionary
                    print(JSON)
                               
                    var strSuccess : String!
                    strSuccess = JSON["status"]as Any as? String
                               
                    var strSuccessAlert : String!
                    strSuccessAlert = JSON["msg"]as Any as? String
                               
                    if strSuccess == "success" {
                               
                        let defaults = UserDefaults.standard
                        defaults.setValue("", forKey: "keyRequestMoney")
                        defaults.setValue(nil, forKey: "keyRequestMoney")
                        
                         self.profileWB()
                                
                        // CRNotifications.showNotification(type: CRNotifications.success, title: "Success!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                           
                        
                        
                        
                    }
                    else {
                                
                        CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                        Utils.RiteVetIndicatorHide()
                        
                    }
                               
                }

                case .failure(_):
                    print("Error message:\(String(describing: response.result.error))")
                               // self.indicator.stopAnimating()
                               // self.enableService()
                    Utils.RiteVetIndicatorHide()
                               
                    let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                               
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                    }
                               
                    alertController.addAction(okAction)
                               
                    self.present(alertController, animated: true, completion: nil)
                               
                    break
            }
        }
    }
    
    
    
    @objc func profileWB() {
       //  Utils.RiteVetIndicatorShow()
           
        let urlString = BASE_URL_aChurchapp
               
        var parameters:Dictionary<AnyHashable, Any>!
           
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any]
         {
             let x : Int = (person["userId"] as! Int)
             let myString = String(x)
            
                parameters = [
                    "action"         : "profile",
                    "userId"         : String(myString)
                ]
         }
                
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               
                               let JSON = data as! NSDictionary
                                  // print(JSON)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                               var strSuccessAlert : String!
                               strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == "success" //true
                               {
                                Utils.RiteVetIndicatorHide()
                                
                                // self.lblBigAmount.text = "$ 0"
                                
                                var dict: Dictionary<AnyHashable, Any>
                                dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(dict, forKey: "keyLoginFullData")
                                
                                
                                
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            
            // let x : NSNumber = person["wallet"] as! NSNumber
            // self.lblTotalAmountInWallet.text = "$ "+"\(x)"
            // self.myCurrentBalance = "\(x)"
           
            
            if person["wallet"] is String {
                print("Yes, it's a String")

                self.lblTotalAmountInWallet.text = "$ "+(person["wallet"] as! String)

            } else if person["wallet"] is Int {
                print("It is Integer")
                            
                let x2 : Int = (person["wallet"] as! Int)
                let myString2 = String(x2)
                self.lblTotalAmountInWallet.text = "$ "+myString2
                            
            } else {
                print("i am number")
                            
                let temp:NSNumber = person["wallet"] as! NSNumber
                let tempString = temp.stringValue
                self.lblTotalAmountInWallet.text = "$ "+tempString
            }
            
            Utils.RiteVetIndicatorHide()
            
            let alert = UIAlertController(title: "Success!", message: String("Money sent successfully."), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in

                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardId") as? Dashboard
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                
            }))
             
            self.present(alert, animated: true)
            
        }
        else {
            self.lblTotalAmountInWallet.text = "$0"
        }
                                
                // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BusinessDashbaordId") as? BusinessDashbaord
                // self.navigationController?.pushViewController(push!, animated: true)
                                
                                
                               }
                               else
                               {
                                   // self.indicator.stopAnimating()
                                   // self.enableService()
                                CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                                   Utils.RiteVetIndicatorHide()
                               }
                               
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               // self.indicator.stopAnimating()
                               // self.enableService()
                               Utils.RiteVetIndicatorHide()
                               
                               let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }
        
        
    
       }
    
    
    
    
    
    
    
    
    
     //MARK:- USERS LIST -
     @objc func payOrDeclineWB(strRequestId:String,strStatus:String) {
         // Utils.RiteVetIndicatorShow()
         
         let defaults = UserDefaults.standard
         let userName = defaults.string(forKey: "KeyLoginPersonal")
         if userName == "loginViaPersonal" {
             // personal user
              Utils.RiteVetIndicatorPesonalShow()
             
         }
         else {
              Utils.RiteVetIndicatorShow()
         }
            
         let urlString = BASE_URL_aChurchapp
                
         var parameters:Dictionary<AnyHashable, Any>!
            
          if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any]
          {
              let x : Int = (person["userId"] as! Int)
              let myString = String(x)
             
             /*
              action: updaterequest
              requestId:
              userId:
              status:  1=Request/ 2=send/ 3=DECLINE
              */
             
                 parameters = [
                     "action"         : "updaterequest",
                     "userId"         : String(myString),
                     "requestId"      : String(strRequestId),
                     "status"         : String(strStatus)
                 ]
          }
                 
                    print("parameters-------\(String(describing: parameters))")
                    
                    Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                        {
                            response in
                
                            switch(response.result) {
                            case .success(_):
                               if let data = response.result.value {

                                
                                let JSON = data as! NSDictionary
                                 print(JSON)
                                 
                                
                                var strSuccess : String!
                                strSuccess = JSON["status"]as Any as? String
                                
                                var strSuccessAlert : String!
                                strSuccessAlert = JSON["msg"]as Any as? String
                                
                                if strSuccess == "success" //true
                                {
                                 Utils.RiteVetIndicatorHide()
                                 
                                 // let defaults = UserDefaults.standard
                                 
                                 
                                  // var ar : NSArray!
                                  // ar = (JSON["data"] as! Array<Any>) as NSArray
                                  // self.arrListOfUsers = (ar as! Array<Any>)
                                 
                                 // print(self.arrListOfUsers as Any)
                                 
                                 
                                 
                                 
                                 
                                 
                                // self.usersListWBForSendOrReceive(strSendOrReceive: "RECEIVE", strForCell: "9")
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 // self.tbleView.delegate = self
                                 // self.tbleView.dataSource = self
                                 // self.tbleView.reloadData()
                 
                                 
                                }
                                else
                                {
                                    // self.indicator.stopAnimating()
                                    // self.enableService()
                                 CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                                    Utils.RiteVetIndicatorHide()
                                }
                                
                            }

                            case .failure(_):
                                print("Error message:\(String(describing: response.result.error))")
                                // self.indicator.stopAnimating()
                                // self.enableService()
                                Utils.RiteVetIndicatorHide()
                                
                                let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                                
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                        UIAlertAction in
                                        NSLog("OK Pressed")
                                    }
                                
                                alertController.addAction(okAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                break
                             }
                        }
         
         
     
        }
    
}


extension RequestMoneyUser: UITableViewDelegate
{
    
}
