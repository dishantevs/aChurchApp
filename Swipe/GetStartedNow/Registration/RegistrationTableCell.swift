//
//  RegistrationTableCell.swift
//  Swipe
//
//  Created by apple on 27/10/21.
//  Copyright © 2021 Apple . All rights reserved.
//

import UIKit

class RegistrationTableCell: UITableViewCell {

    @IBOutlet weak var txtFirstName:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtFirstName,
                              tfName: txtFirstName.text!,
                              tfCornerRadius: 6,
                              tfpadding: 40,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .lightGray,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "First Name")
        }
    }
    
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtEmail,
                              tfName: txtEmail.text!,
                              tfCornerRadius: 6,
                              tfpadding: 40,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .lightGray,
                              tfAppearance: .dark,
                              tfKeyboardType: .emailAddress,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Email Address")
        }
    }
    
    @IBOutlet weak var txtPassword:UITextField! {
        didSet {
            // txtPassword.isSecureTextEntry = true
            Utils.textFieldUI(textField: txtPassword,
                              tfName: txtPassword.text!,
                              tfCornerRadius: 6,
                              tfpadding: 40,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .lightGray,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Password")
        }
    }
    
    @IBOutlet weak var txtAddress:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtAddress,
                              tfName: txtAddress.text!,
                              tfCornerRadius: 6,
                              tfpadding: 40,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .lightGray,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Address")
        }
    }
    
    @IBOutlet weak var txtPhone:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtPhone,
                              tfName: txtPhone.text!,
                              tfCornerRadius: 6,
                              tfpadding: 44,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .lightGray,
                              tfAppearance: .dark,
                              tfKeyboardType: .phonePad,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Phone Number")
        }
    }
    
    @IBOutlet weak var btnDOB:UIButton!
    @IBOutlet weak var txtDOB:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtDOB,
                              tfName: txtDOB.text!,
                              tfCornerRadius: 6,
                              tfpadding: 44,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .lightGray,
                              tfAppearance: .dark,
                              tfKeyboardType: .phonePad,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "DOB")
        }
    }
    
    @IBOutlet weak var txtSSN:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtSSN,
                              tfName: txtSSN.text!,
                              tfCornerRadius: 6,
                              tfpadding: 44,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .lightGray,
                              tfAppearance: .dark,
                              tfKeyboardType: .phonePad,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Social Security Number")
        }
    }
    
    @IBOutlet weak var btnUploadDocuments:UIButton!
    @IBOutlet weak var txtUploadDocuments:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtUploadDocuments,
                              tfName: txtUploadDocuments.text!,
                              tfCornerRadius: 6,
                              tfpadding: 44,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .lightGray,
                              tfAppearance: .dark,
                              tfKeyboardType: .phonePad,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Upload Document")
        }
    }
    
    // @IBOutlet weak var btnCheckUncheck:UIButton!
    
    @IBOutlet weak var btnAlreadyHaveAnAccount:UIButton! {
        didSet {
            btnAlreadyHaveAnAccount.setTitle("Already have an account - Sign In", for: .normal)
            btnAlreadyHaveAnAccount.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBOutlet weak var btnSignUp:UIButton! {
        didSet {
            btnSignUp.backgroundColor = .black
            btnSignUp.layer.cornerRadius = 6
            btnSignUp.setTitleColor(.white, for: .normal)
            btnSignUp.setTitle("Sign Up", for: .normal)
        }
    }
    
    @IBOutlet weak var lblAreYouOverTheAge:UILabel! {
        didSet {
            lblAreYouOverTheAge.textColor = .black
            lblAreYouOverTheAge.text =  "Are you over the age of 18+ year ?"
        }
    }
    
    @IBOutlet weak var imgUpload:UIImageView! {
        didSet {
            imgUpload.layer.cornerRadius = 8
            imgUpload.clipsToBounds = true
            imgUpload.layer.borderColor = UIColor.lightGray.cgColor
            imgUpload.layer.borderWidth = 0.8
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
