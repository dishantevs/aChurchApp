//
//  Registration.swift
//  TheGlobalHair
//
//  Created by Apple on 18/11/20.
//

import UIKit
import Alamofire

// MARK:- LOCATION -
import CoreLocation

class Registration: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var str_check_upload_Section_select:String! = "0"
    
    var str_type_one : String!
    
    // image
    var check_image_status:String! = "0"
    var imageStr:String!
    var imgData:Data!
    
    // ***************************************************************** // nav
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = .white
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Registraiton"
            lblNavigationTitle.textColor = .white
        }
    }
    
    // ***************************************************************** // nav
    
    var profilePart:String!
    
    var myDeviceTokenIs:String!
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            tbleView.backgroundColor = .white
            tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    @IBOutlet weak var viewBGloginDetails:UIView! {
        didSet {
            viewBGloginDetails.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var btnUpperSignIn:UIButton! {
        didSet {
            btnUpperSignIn.backgroundColor = .clear
            btnUpperSignIn.setTitle("Sign In", for: .normal)
            btnUpperSignIn.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnUpperSignUp:UIButton! {
        didSet {
            btnUpperSignUp.backgroundColor = .clear
            btnUpperSignUp.setTitle("Sign Up", for: .normal)
            btnUpperSignUp.setTitleColor(.black, for: .normal)
            
            let lineView = UIView(frame: CGRect(x: 0, y: btnUpperSignUp.frame.size.height, width: btnUpperSignUp.frame.size.width, height: 2))
            lineView.backgroundColor = .black
            btnUpperSignUp.addSubview(lineView)
        }
    }
    
    /*@IBOutlet weak var txtRegistrationAs:UITextField! {
     didSet {
     Utils.textFieldUI(textField: txtRegistrationAs,
     tfName: txtRegistrationAs.text!,
     tfCornerRadius: 6,
     tfpadding: 40,
     tfBorderWidth: 0.8,
     tfBorderColor: .lightGray,
     tfAppearance: .dark,
     tfKeyboardType: .default,
     tfBackgroundColor: .white,
     tfPlaceholderText: "Registration as")
     }
     }*/
    
    
    
    @IBOutlet weak var btnRegistrationAs:UIButton!
    
    // ***************************************************************** //
    // ***************************************************************** //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        //self.txtRegistrationAs.delegate = self
        
        
        // self.btnUpperSignIn.addTarget(self, action: #selector(upperSignInClickMethod), for: .touchUpInside)
        // self.btnAlreadyHaveAnAccount.addTarget(self, action: #selector(upperSignInClickMethod), for: .touchUpInside)
        
        // self.btnSignUp.addTarget(self, action: #selector(signUpClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        /*NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)*/
        
        // self.btnCheckUncheck.setImage(UIImage(named: "uncheck3"), for: .normal)
        // self.btnCheckUncheck.tag = 0
        // self.btnCheckUncheck.addTarget(self, action: #selector(checkUncheckClickMethod), for: .touchUpInside)
        
        
        
        // self.btnRegistrationAs.addTarget(self, action: #selector(registrationClickMethod), for: .touchUpInside)
        
        // self.iAmHereForLocationPermission()
    }
    
    @objc func registrationClickMethod() {
        
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPurchaseTableCell
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            
            
            // self.findMyStateTaxWB()
        }
    }
    
    @objc func upperSignInClickMethod() {
        self.navigationController?.popViewController(animated: false)
    }
    
    /*@objc func checkUncheckClickMethod() {
     if self.btnCheckUncheck.tag == 1 {
     
     self.btnSignUp.isUserInteractionEnabled = false
     self.btnSignUp.backgroundColor = .lightGray
     self.btnSignUp.setTitleColor(.black, for: .normal)
     self.btnCheckUncheck.setImage(UIImage(named: "uncheck3"), for: .normal)
     self.btnCheckUncheck.tag = 0
     
     } else {
     
     self.btnSignUp.isUserInteractionEnabled = true
     self.btnSignUp.backgroundColor = APP_BUTTON_COLOR
     self.btnSignUp.setTitleColor(.white, for: .normal)
     self.btnCheckUncheck.setImage(UIImage(named: "uncheck2"), for: .normal)
     self.btnCheckUncheck.tag = 1
     
     }
     }*/
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: false)
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    @objc func signUpClickMethod() {
        
        if self.profilePart == "Member" {
            
            self.signUpValidationCheck()
            
        } else {
            
            self.signUpValidationCheck()
            
            // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompleteProfileId") as? CompleteProfile
            // self.navigationController?.pushViewController(push!, animated: true)
            
        }
        
    }
    
    
    // MARK:- CHECK VALIDATION BEFORE REGISTRATION -
    @objc func signUpValidationCheck() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        if cell.txtFirstName.text! == "" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Name should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtEmail.text! == "" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Email should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtPassword.text! == "" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Password should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtPhone.text! == "" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Phone should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtAddress.text! == "" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("Address should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtDOB.text! == "" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("DOB should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtSSN.text! == "" {
            
            let alert = UIAlertController(title: String("Error!"), message: String("SSN should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            // MARK:- CALL WEBSERVICE OF SIGN UP WHEN TEXT FIELDS IS NOT EMPTY -
            
            if self.check_image_status == "0" {
                
                let alert = UIAlertController(title: String("Error!"), message: String("Please upload one Document to procced further."), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
                self.registration_via_image_wb()
                
            }
            
        }
    }
    
    @objc func registration_via_image_wb() {
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        let indexPath1 = IndexPath.init(row: 1, section: 0)
        let cell1 = self.tbleView.cellForRow(at: indexPath1) as! RegistrationTableCell
        
        
        // Create UserDefaults
        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "deviceFirebaseToken") {
            myDeviceTokenIs = myString
            
        }
        else {
            myDeviceTokenIs = "111111111111111111111"
        }
        
        
        
        if self.str_check_upload_Section_select == "0" {
            self.str_type_one = "none"
        } else if self.str_check_upload_Section_select == "1" {
            self.str_type_one = "Social Security Card"
        } else if self.str_check_upload_Section_select == "2" {
            self.str_type_one = "Driving Licence"
        } else if self.str_check_upload_Section_select == "3" {
            self.str_type_one = "Passport"
        }
        
        // var parameters:Dictionary<AnyHashable, Any>!
        let parameters = [
            "action"        : "registration",
            "username"      : String(cell.txtFirstName.text!),
            "fullName"      : String(cell.txtFirstName.text!),
            "email"         : String(cell.txtEmail.text!),
            "password"      : String(cell.txtPassword.text!),
            "contactNumber" : String(cell.txtPhone.text!),
            "ssn"           : String(cell.txtSSN.text!),
            "dob"           : String(cell.txtDOB.text!),
            "address"       : String(cell.txtAddress.text!),
            "document_type" : (str_type_one!),
            "deviceToken"   : (myDeviceTokenIs!),
            "device"        : String("iOS"),
            "role"          : String("Member")
        ]
        
        print(parameters as Any)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(self.imgData, withName: "documentUpload",fileName: "UploadRegistrationDocument.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                
                // let paramsData:Data = NSKeyedArchiver.archivedData(withRootObject: value)
                
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:BASE_URL_aChurchapp)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //print("Upload Progress: \(progress.fractionCompleted)")
                    
                    let alertController = UIAlertController(title: "Uploading image", message: "Please wait......", preferredStyle: .alert)
                    
                    let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)
                    
                    progressDownload.setProgress(Float((progress.fractionCompleted)/1.0), animated: true)
                    progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
                    
                    alertController.view.addSubview(progressDownload)
                    self.present(alertController, animated: true, completion: nil)
                })
                
                upload.responseJSON { response in
                    
                    //print(response.result.value as Any)
                    if let data = response.result.value {
                        let JSON = data as! NSDictionary
                        print(JSON)
                        
                        if (JSON["status"] as! String) == "Fails" {
                            
                            let alert = UIAlertController(title: String("Alert"), message: (JSON["msg"] as! String), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        } else {
                            self.dismiss(animated: true, completion: nil)
                            
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                            let defaults = UserDefaults.standard
                            defaults.setValue(dict, forKey: "keyLoginFullData")
                            
                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompleteProfileUploadPictureId") as? CompleteProfileUploadPicture
                            self.navigationController?.pushViewController(settingsVCId!, animated: true)
                            
                        }
                        
                        
                        // self.dismiss(animated: true, completion: nil)
                    }
                    else {
                        // CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:"Server Not Responding. Please try again Later.", dismissDelay: 1.5, completion:{})
                        
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                self.dismiss(animated: true, completion: nil)
            }}
        
    }
    
    @objc func dobClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        RPicker.selectDate(title: "Select Date", didSelectDate: {[] (selectedDate) in
            // TODO: Your implementation for date
            cell.txtDOB.text = selectedDate.dateString("dd-MM-yyyy")
        })
        
    }
    
    @objc func upload_document() {
        
        let alert = UIAlertController(title: "Upload Documents", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Social Security Card", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.str_check_upload_Section_select = "1"
            self.tbleView.reloadData()
            
            self.open_camera_gallery(str_title: "Social Security Card")
        }))
        
        alert.addAction(UIAlertAction(title: "Driving Licence", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.str_check_upload_Section_select = "2"
            self.tbleView.reloadData()
            
            self.open_camera_gallery(str_title: "Driving Licence")
        }))
        
        alert.addAction(UIAlertAction(title: "Passport", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.str_check_upload_Section_select = "3"
            self.tbleView.reloadData()
            
            self.open_camera_gallery(str_title: "Passport")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        
        
    }
    
    @objc func open_camera_gallery(str_title:String) {
        
        let alert = UIAlertController(title: "Upload "+String(str_title), message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        
        
    }
    
    
    @objc func openCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func openGallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let indexPath = IndexPath.init(row: 1, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        cell.imgUpload.image = image_data
        let imageData:Data = image_data!.pngData()!
        imageStr = imageData.base64EncodedString()
        self.dismiss(animated: true, completion: nil)
        
        imgData = image_data!.jpegData(compressionQuality: 0.2)!
        
        self.check_image_status = "1"
    }
    
}

extension Registration: UITableViewDataSource , UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell:RegistrationTableCell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! RegistrationTableCell
            
            cell.backgroundColor = .white
            
            let backgroundView = UIView()
            backgroundView.backgroundColor = .clear
            cell.selectedBackgroundView = backgroundView
            
            cell.txtFirstName.delegate = self
            cell.txtEmail.delegate = self
            cell.txtPassword.delegate = self
            cell.txtPhone.delegate = self
            
            // cell.btnSignUp.isUserInteractionEnabled = true
            // cell.btnSignUp.backgroundColor = .black
            // cell.btnSignUp.setTitleColor(.white, for: .normal)
            
            if self.str_check_upload_Section_select == "0" {
                
                cell.txtUploadDocuments.placeholder = "Upload Document"
                
            } else if self.str_check_upload_Section_select == "1" {
                
                cell.txtUploadDocuments.text = "Upload Social Security Number and Card"
                
            } else if self.str_check_upload_Section_select == "2" {
                
                cell.txtUploadDocuments.text = "Upload Driving Licence"
                
            } else {
                
                cell.txtUploadDocuments.text = "Upload Passport"
                
            }
            
            cell.btnDOB.addTarget(self, action: #selector(dobClickMethod), for: .touchUpInside)
            cell.btnUploadDocuments.addTarget(self, action: #selector(upload_document), for: .touchUpInside)
            
            return cell
            
        } else if indexPath.row == 1 {
            
            let cell:RegistrationTableCell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! RegistrationTableCell
            
            cell.backgroundColor = .white
            
            let backgroundView = UIView()
            backgroundView.backgroundColor = .clear
            cell.selectedBackgroundView = backgroundView
            
            return cell
            
        } else {
            
            let cell:RegistrationTableCell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! RegistrationTableCell
            
            cell.backgroundColor = .white
            
            let backgroundView = UIView()
            backgroundView.backgroundColor = .clear
            cell.selectedBackgroundView = backgroundView
            
            cell.btnSignUp.addTarget(self, action: #selector(signUpClickMethod), for: .touchUpInside)
            
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            return 660
            
        } else if indexPath.row == 1 {
            
            if self.str_check_upload_Section_select == "0" {
                return 0
            } else {
                return 170
            }
            
        } else {
            
            return 200
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
    }
    
}


extension Date {
    
    func dateString(_ format: String = "dd-MM-yyyy, hh:mm a") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    func dateByAddingYears(_ dYears: Int) -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.year = dYears
        
        return Calendar.current.date(byAdding: dateComponents, to: self)!
    }
}
