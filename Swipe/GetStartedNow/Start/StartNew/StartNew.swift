//
//  StartNew.swift
//  Swipe
//
//  Created by evs_SSD on 1/28/20.
//  Copyright © 2020 Apple . All rights reserved.
//

import UIKit

class StartNew: UIViewController {

    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "WELCOME"
            lblNavigationTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var imgBusinessImage:UIImageView!
    @IBOutlet weak var imgpersonalImage:UIImageView!
    
    @IBOutlet weak var btnGetStartedNow:UIButton! {
        didSet {
            btnGetStartedNow.layer.cornerRadius = 6
            btnGetStartedNow.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblWelcomeMessage:UILabel! {
        didSet {
            lblWelcomeMessage.text = "Hello, Welcome to the +a ChurchApp\nContinue your account."
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnGetStartedNow.addTarget(self, action: #selector(getStartedNowClickMethod), for: .touchUpInside)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @objc func getStartedNowClickMethod() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GetStartedNowId") as? GetStartedNow
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    

}
