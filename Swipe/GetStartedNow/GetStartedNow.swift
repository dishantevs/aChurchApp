//
//  GetStartedNow.swift
//  KREASE
//
//  Created by Apple  on 16/10/19.
//  Copyright © 2019 Apple . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import CRNotifications

class GetStartedNow: UIViewController,UIScrollViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate {

     var myDeviceTokenIs:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
        }
    }

    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "GET STARTED NOW"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = .white
         }
    }
    
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            txtEmail.layer.cornerRadius = 6
            txtEmail.clipsToBounds = true
            txtEmail.setLeftPaddingPoints(40)
            txtEmail.layer.borderColor = UIColor.clear.cgColor
            txtEmail.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtPassword:UITextField! {
        didSet {
            txtPassword.layer.cornerRadius = 6
            txtPassword.clipsToBounds = true
            txtPassword.setLeftPaddingPoints(40)
            txtPassword.layer.borderColor = UIColor.clear.cgColor
            txtPassword.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var btnSignUp:UIButton!
    @IBOutlet weak var btnForgotPassword:UIButton!
    
    @IBOutlet weak var btnSignInSubmit:UIButton! {
        didSet {
            btnSignInSubmit.backgroundColor = NAVIGATION_BUSINESS_BACKGROUND_COLOR
            btnSignInSubmit.layer.cornerRadius = 6
            btnSignInSubmit.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
         btnSignUp.addTarget(self, action: #selector(signUpClickMethod), for: .touchUpInside)
         btnSignInSubmit.addTarget(self, action: #selector(signInValidationCheck), for: .touchUpInside)
        btnForgotPassword.addTarget(self, action: #selector(forgotPasswordClickMethod), for: .touchUpInside)
        
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
          self.rememberMe()
    }
    
    @objc func rememberMe() {
        // MARK:- PUSH -
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
                
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardId") as? Dashboard
            self.navigationController?.pushViewController(push!, animated: true)
            
            
        }
    }
    
    /*
    @objc func pushToCompleteprofile() {
    
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDStateCountryCityId") as? PDStateCountryCity
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    // MARK:- SIGN UP CLICK -
    @objc func signUpClickMethod() {
         let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegistrationId") as? Registration
         self.navigationController?.pushViewController(push!, animated: true)
    }
    
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK:- FORGOT PASSWORD -
    @objc func forgotPasswordClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordId") as? ForgotPassword
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- CHECK VALIDATION BEFORE LOGIN -
    @objc func signInValidationCheck() {
        
        /*if self.txtEmail.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Email should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if self.txtPassword.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Password should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else {*/
            // MARK:- CALL WEBSERVICE OF SIGN IN WHEN TEXT FIELDS IS NOT EMPTY -
            self.signInSubmitClickMethod()
        // }
        
    }
    
    @objc func signInSubmitClickMethod() {
        self.view.endEditing(true)
        
        let urlString = BASE_URL_aChurchapp
        
        var parameters:Dictionary<AnyHashable, Any>!
        
        
        Utils.RiteVetIndicatorShow()
        parameters = [
            "action"        : "login",
            "email"         : String(txtEmail.text!),
            "password"      : String(txtPassword.text!)
        ]
             
        print("parameters-------\(String(describing: parameters))")
                   
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
            {
                response in
               
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value {

                               
                        let JSON = data as! NSDictionary
                         print(JSON)
                               
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                               
                        var strSuccessAlert : String!
                        strSuccessAlert = JSON["msg"]as Any as? String
                               
                        if strSuccess == "success" {
                            
                                /*
                                 data =     {
                                     OTP = "";
                                     address = "";
                                     contactNumber = 9856325698;
                                     device = Android;
                                     deviceToken = "";
                                     email = "ios@gmail.com";
                                     firebaseId = "";
                                     fullName = ios1;
                                     image = "";
                                     lastName = "";
                                     role = Member;
                                     socialId = "";
                                     socialType = "";
                                     userId = 44;
                                     wallet = 0;
                                     zipCode = "";
                                 };
                                 msg = "Data save successfully.";
                                 status = success;
                                 */
                            
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                   
                            let defaults = UserDefaults.standard
                            defaults.setValue(dict, forKey: "keyLoginFullData")
                                   
                            CRNotifications.showNotification(type: CRNotifications.success, title: "Message!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                                
                                
                            var strSuccess2 : String!
                            strSuccess2 = dict["role"] as Any as? String
                                
                                // print(strSuccess2 as Any)
                                
                            if strSuccess2 == "Member" {
                                    
                                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardId") as? Dashboard
                                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                                
                            }
                            else {
                                    // self.loginWBClickMethod()
                            }
                                 
                            Utils.RiteVetIndicatorHide()
                            
                        }
                        else {
                                   // self.indicator.stopAnimating()
                                   // self.enableService()
                            CRNotifications.showNotification(type: CRNotifications.error, title: "Error!", message:strSuccessAlert, dismissDelay: 1.5, completion:{})
                            Utils.RiteVetIndicatorHide()
                        }
                               
                    }

                case .failure(_):
                    print("Error message:\(String(describing: response.result.error))")
                               // self.indicator.stopAnimating()
                               // self.enableService()
                    Utils.RiteVetIndicatorHide()
                               
                    let alertController = UIAlertController(title: nil, message: SERVER_ISSUE_MESSAGE_ONE+"\n"+SERVER_ISSUE_MESSAGE_TWO, preferredStyle: .actionSheet)
                               
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                    }
                               
                    alertController.addAction(okAction)
                               
                    self.present(alertController, animated: true, completion: nil)
                               
                    break
                }
        }
    
    }
    
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
